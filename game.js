//Carrot Invaders - game.js - Kieran Dunbar (kid10)
//First let's setup our variables to access the canvas and drawing context
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var bounding_box = canvas.getBoundingClientRect();

//Also setup our event handlers
canvas.addEventListener("mousemove", mouseMove);
canvas.addEventListener("click", mouseClick);
canvas.addEventListener("mousedown", function(mouse_event) { mouse_event.preventDefault(); }); //Prevent annoying select issue
canvas.addEventListener("contextmenu", function(mouse_event) { mouse_event.preventDefault(); }); //Stop right clicks, may use this later on
document.addEventListener("keydown", keyDown);
document.addEventListener("keyup", keyUp);

//And our game variables
var current_screen = 0; //0 = Menu, 1 = Main Screen, 2 = Pause, 3 = Game Over, 4 = Help, 5 = Ship Select, 6 = Intro
var score = 0;
var health = 100; //In percent
var ship_type = 5;
var ship_color = 0;
var ship_colors = [{name: "Grey", code: "#929292"}, {name: "Blue", code: "#25afff"}, {name: "Red", code: "#ff2525"}, 
{name: "Yellow", code: "#fcff25"}, {name: "Green", code: "#25ff25"}];
var current_level = 0;
var showNewLevelTimeout = 0;
var newHighScore = -1;

var sound_enabled = true;
var music_enabled = true;

var bullets = [];
var carrots = [];
var explosions = [];

/*
 * Level Configuration
 * | max_carrots - The maximum number of carrots on screen at any one time
 * | carrot_prob - The probability of a new carrot being created on screen
 * | carrot_speed - The speed factor of carrots, the higher the number, the faster the carrots
 * | health_points - The number of planet health points to decrease on a collision with a carrot
 * | points_required - The total number of points required to complete the level (not used on last level)
 */
var levels = [{
	max_carrots: 8,
	carrot_prob: 0.01,
	carrot_speed: 0.75,
	health_points: 7.5,
	points_required: 500
}, {
	max_carrots: 12,
	carrot_prob: 0.015,
	carrot_speed: 0.75,
	health_points: 7.5,
	points_required : 1500
}, {
	max_carrots: 16,
	carrot_prob: 0.02,
	carrot_speed: 0.85,
	health_points: 10,
	points_required: 3000
}];

//Debugging Options
var debug_info = false;
var bounding_boxes = false;

//Function called on load to start the game
function startGame() {
	//Draw the screen and start the animations
	drawScreen();
	window.requestAnimationFrame(runAnimations);
}

//At each frame, draw the screen
function runAnimations() {
	drawScreen();
	window.requestAnimationFrame(runAnimations);
}

//Core drawing function
function drawScreen() {	
	//Reset the canvas
	canvas.width = canvas.width;
	context.fillStyle = '#fff';
	context.strokeStyle = '#fff';
	
	//Start off by drawing the centre planet (as long as we're not on the help or game over screen)
	if(current_screen < 3) {
		drawImage(planet);
		if(bounding_boxes) {
			context.beginPath();
			context.arc((canvas.width/2), (canvas.height/2), (planet.height/2), 0, 2*Math.PI, false);
			context.stroke();
			context.closePath();
		}
	}
	
	switch(current_screen) {
		//Menu Screen
		case 0:
			drawMenu();
		break;
		//Main Screen
		case 1:
			drawMain();
		break;
		//Pause Screen
		case 2:
			drawPause();
			drawStats();
		break;
		//Game Over Screen
		case 3:
			drawGameOver();
		break;
		//Help Screen
		case 4: 
			drawHelp();
		break;
		//Ship Select Screen
		case 5:
			drawShipSelect();
		break;
		//Intro Screen
		case 6:
			drawIntro();
		break;
	}
	
	//Draw the top-right buttons (music/sound etc.)
	if(music_enabled) {
		if(music.paused) music.play();
		drawImage(music_button_on);
	} else {
		if(!music.paused) music.pause();
		drawImage(music_button_off);
	}
	
	if(sound_enabled) {
		drawImage(sound_button_on);
	} else {
		drawImage(sound_button_off);
	}
	
	//Also show debug info if needed
	if(debug_info) {		
		drawText("X: " + mousex, 10, 20, 11, "start");
		drawText("Y: " + mousey, 10, 35, 11, "start");
		drawText("Angle: " + (mouseangle * (180/Math.PI)).toFixed(2) + " degrees", 10, 50, 11, "start");
		drawText("Num. Bullets: " + bullets.length, 10, 65, 11, "start");
		drawText("Num. Carrots: " + carrots.length, 10, 80, 11, "start");
		drawText("Level: " + current_level, 10, 95, 11, "start");
		drawText("Ship Type: " + ship_type, 10, 110, 11, "start");
		drawText("Ship Colour: " + ship_colors[ship_color].name, 10, 125, 11, "start");
	}
}

//Menu Screen
function drawMenu() {	
	drawText("Carrot", (canvas.width/2)-110, 75, 50, "start");
	drawText("Invaders", (canvas.width/2)-30, 115, 50, "start");
	
	drawButton(start_button);
	drawButton(help_button);
	
	//Put a copyright in the bottom right corner - http://stackoverflow.com/questions/8095624/how-to-add-copyright-symbol-in-a-highcharts-title
	drawText("\u00A9 Kieran Dunbar 2015", canvas.width-5, canvas.height-10, 15, "right");
}

//Main Screen
function drawMain() {
	//If we have no health left, it's game over
	if(health <= 0)
	{
		current_screen = 3;
	}
	
	//Check the level we're on, and increment if necessary (and possible)
	if(levels[current_level].points_required <= score && levels.length > current_level + 1) {
		current_level++;
		showNewLevelTimeout = 250;
	}
	
	//If we're showing new level text
	if(showNewLevelTimeout > 0) {
		drawText("Level " + (current_level + 1), (canvas.width/2), 75, 40);
		showNewLevelTimeout--;
	}
	
	//Check for any key presses (captured in controls)
	checkKeys();

	//And then draw everything
	drawShip();
	drawBullets();
	drawCarrots();
	drawExplosions();
	drawStats();
	drawImage(pause_button);
}

//Function to draw the game stats on the canvas (health and score etc.)
function drawStats() {
	context.strokeStyle = '#fff';
	context.strokeRect(canvas.width-135, 60, 120, 25);
	
	context.fillStyle = "#ff0000";
	context.fillRect(canvas.width-132, 63, health * 1.14, 19);

	drawText("Planet Health", canvas.width-122.5, 78, 16, "start");
	drawText(score, canvas.width-10, 120, 35, "right");
}

//Pause Screen
function drawPause() {
	drawText("Paused", (canvas.width/2), 75, 40);
	
	drawButton(resume_button);
	drawButton(exit_button);
}

//Game Over Screen
function drawGameOver() {
	drawText("Game Over!", (canvas.width/2)-60, 75, 40);
	
	drawImage(dead_rabbit);
	
	drawText("Score: " + score, (canvas.width/2)-60, 110, 30);
	
	drawButton(retry_button);
	drawButton(exit_button);
	
	//Draw the high scores
	drawHighScores();
}

//Function to draw the current High Scores
function drawHighScores() {	
	drawText("High Scores", (canvas.width/2), 180, 30);
	context.strokeRect((canvas.width/2)-150, 200, 300, 250);
	context.beginPath();
	context.moveTo((canvas.width/2)-100, 200);
	context.lineTo((canvas.width/2)-100, 450);
	context.stroke();
	
	for(i = 0; i < 5; i++)
	{
		var y = 235 + (i * 50);
		
		//First get the high score from local storage
		var highscore = parseInt(localStorage["ci-highscore" + i]) || "-";
		
		//Then, if we haven't already added our current score, if it's beaten the current high score, add it in
		if(newHighScore == -1 && (highscore == "-" || score > highscore)) {
			highscore = score;
			localStorage["ci-highscore" + i] = score;
			newHighScore = i;
		}
		
		//Highlight our new score (if we have one)
		if(newHighScore == i) {
			context.fillStyle = "rgba(255, 255, 0, 0.5)";
			context.fillRect((canvas.width/2)-149, y-34, 298, 48);
		}
		
		//Then output them in a grid
		drawText(i+1, (canvas.width/2)-133, y-2, 25, "start");
		drawText(highscore, (canvas.width/2)-85, y, 30, "start");
		
		if(i < 4) {
			context.beginPath();
			context.moveTo((canvas.width/2)-150, y+15);
			context.lineTo((canvas.width/2)+150, y+15);
			context.stroke();
		}
	}
}

//Help Screen
function drawHelp() {
	//Draw the title
	drawText("Help", (canvas.width/2), 90, 40);
	
	//Describe the aim of the game
	drawText("The aim of the game is to stop the carrots from invading the Rabbit's planet in the centre of the screen.", (canvas.width/2), 130, 18);
	drawText("Each time a carrot hits the planet, it's health will decrease until it's game over!", (canvas.width/2), 150, 18);
	
	//Then show the controls
	drawText("You can control the game using either your mouse or keyboard:", (canvas.width/2), 200, 18);
	
	drawImage(mouse_image);
	drawText("Move your mouse around the planet to move", 50, 475, 18, "start");
	drawText("your spaceship. And left click to fire at", 50, 500, 18, "start");
	drawText("any incoming carrots.", 50, 525, 18, "start");
	
	drawImage(keyboard_image);
	drawText("Use the left/right arrow keys (or A/D) to move", (canvas.width/2)+120, 460, 18, "start");
	drawText("your spaceship. And the up/down arrow keys", (canvas.width/2)+120, 485, 18, "start");
	drawText("(or W/S) to swap sides. Use the spacebar to", (canvas.width/2)+120, 510, 18, "start");
	drawText("fire at any incoming carrots.", (canvas.width/2)+120, 535, 18, "start");
	
	drawButton(back_button);
}

//Ship Select Screen
function drawShipSelect() {
	context.strokeStyle = '#fff';
	context.fillStyle = '#fff';
	
	//Draw the title
	drawText("Select your ship", (canvas.width/2), 110, 40);
	
	//Draw the arrow selectors
	drawButton(next_ship_button);
	context.beginPath();
	context.moveTo((canvas.width/2)-120, 230);
	context.lineTo((canvas.width/2)-120, 270);
	context.lineTo((canvas.width/2)-150, 250);
	context.fill();
	
	drawButton(prev_ship_button);
	context.beginPath();
	context.moveTo((canvas.width/2)+115, 230);
	context.lineTo((canvas.width/2)+115, 270);
	context.lineTo((canvas.width/2)+145, 250);
	context.fill();
	
	drawButton(next_color_button);
	context.beginPath();
	context.moveTo((canvas.width/2)-160, 367.5);
	context.lineTo((canvas.width/2)-160, 392.5);
	context.lineTo((canvas.width/2)-185, 380);
	context.fill();
	
	drawButton(prev_color_button);
	context.beginPath();
	context.moveTo((canvas.width/2)+155, 367.5);
	context.lineTo((canvas.width/2)+155, 392.5);
	context.lineTo((canvas.width/2)+180, 380);
	context.fill();
	
	//Draw the ship
	drawShipImage(canvas.width/2-55, 190, 110);
	
	//Then draw the colours
	for(c = 0; c < ship_colors.length; c++)
	{
		if(c == ship_color) {
			context.fillStyle = "#fff";
			context.fillRect((canvas.width/2)-118 + (48 * c), 358, 44, 44);
		}
		
		context.fillStyle = ship_colors[c].code;
		context.fillRect((canvas.width/2)-116 + (48 * c), 360, 40, 40);
	}
	
	//And finally draw the buttons
	drawButton(select_ship_button);
	drawButton(back_button_ss);
}

//Intro Screen
function drawIntro() {	
	//Draw the main image
	drawImage(intro_image);
	
	//The main text
	drawText("For thousands of years, carrots had been a rabbit's best friend.", (canvas.width/2), 350, 28);
	drawText("Each lived together in peace and harmony.", (canvas.width/2), 380, 28);
	drawText("Until one day, the evil carrots invaded...", (canvas.width/2), 410, 28);
	
	//And finally draw the buttons
	drawButton(continue_button);
	drawButton(back_button_intro);
}

//Function to reset all of the game variables
function resetGame() {
	current_level = 0;
	bullets = [];
	carrots = [];
	score = 0;
	health = 100;
	newHighScore = -1;
	showNewLevelTimeout = 0;
}