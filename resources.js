//Carrot Invaders - resources.js - Kieran Dunbar (kid10)
//Let's define all of our images, sounds and buttons (along with their positions etc.) here.

//The centre planet
var planet = new Image();
planet.onload = function() {
	planet.posx = (canvas.width/2)-(planet.width/2);
	planet.posy = (canvas.height/2)-(planet.height/2);
}
planet.src = "images/planet.png";

//Define all of the image buttons (music, sound and pause)
var music_button_on = new Image();
music_button_on.name = "music_button_on";
music_button_on.src = "images/music.png";
music_button_on.posx = canvas.width-100;
music_button_on.posy = 10;

var music_button_off = new Image();
music_button_off.name = "music_button_off";
music_button_off.src = "images/music_off.png";
music_button_off.posx = canvas.width-100;
music_button_off.posy = 10;

var sound_button_on = new Image();
sound_button_on.name = "sound_button_on";
sound_button_on.src = "images/sound.png";
sound_button_on.posx = canvas.width-50;
sound_button_on.posy = 10;

var sound_button_off = new Image();
sound_button_off.name = "sound_button_off";
sound_button_off.src = "images/sound_off.png";
sound_button_off.posx = canvas.width-50;
sound_button_off.posy = 10;

var pause_button = new Image();
pause_button.name = "pause_button";
pause_button.src = "images/pause.png";
pause_button.posx = canvas.width-140;
pause_button.posy = 14;
pause_button.screen = 1;

//Load in all of the ship colors
var ship_sprites_grey = new Image();
ship_sprites_grey.src = "images/ships_grey.png";

var ship_sprites_blue = new Image();
ship_sprites_blue.src = "images/ships_blue.png";

var ship_sprites_red = new Image();
ship_sprites_red.src = "images/ships_red.png";

var ship_sprites_yellow = new Image();
ship_sprites_yellow.src = "images/ships_yellow.png";

var ship_sprites_green = new Image();
ship_sprites_green.src = "images/ships_green.png";

var ship_sprites = [ship_sprites_grey, ship_sprites_blue, ship_sprites_red, ship_sprites_yellow, ship_sprites_green];

//Then the bullet sprites
var bullet_sprites = new Image();
bullet_sprites.src = "images/bullets.png";

//And our carrot image
var carrot = new Image();
carrot.src = "images/carrot.png";

//And finally the explosion image
var explosion_image = new Image();
explosion_image.src = "images/explosion.png";

//Main image for intro screen
var intro_image = new Image();
intro_image.src = "images/intro.png";
intro_image.posx = 360;
intro_image.posy = 100;

//Dead rabbit for game over screen
var dead_rabbit = new Image();
dead_rabbit.src = "images/dead_rabbit.png";
dead_rabbit.posx = canvas.width/2 + 80;
dead_rabbit.posy = 30;

//Mouse and keyboard for help screen
var mouse_image = new Image();
mouse_image.src = "images/mouse.png";
mouse_image.posx = 100;
mouse_image.posy = 240;

var keyboard_image = new Image();
keyboard_image.src = "images/keyboard.png";
keyboard_image.posx = canvas.width/2+190;
keyboard_image.posy = 240;

//Then our sounds (created using http://www.bfxr.net/)
//1,1,,0.34,0.165,0.25,0.22,0.8118,0.2,-0.3399,-0.01,0.065,0.275,,,,-0.03,,,,0.3787,0.1354,,-0.005,,1,0.03,,,,,,masterVolume
var shoot_sound = new Audio("audio/shoot.mp3");

//5,1,,0.385,1,0.305,0.165,0.365,,-0.25,0.01,,,,,,,,,,,,,0.03,-0.2983,0.14,,,,,,,masterVolume
var explosion_sound = new Audio("audio/explosion.mp3");

//Setup our music, making sure it loops (from http://www.nosoapradio.us)
var music = new Audio("audio/music.mp3");
music.loop = true;
music.volume = 0.5;

//Menu Screen Buttons
var start_button = {name: "start_button", posx: (canvas.width/2)-160, posy: canvas.height-105, width: 150, height: 60, screen: 0, text: "Start"};
var help_button = {name: "help_button", posx: (canvas.width/2)+5, posy: canvas.height-105, width: 150, height: 60, screen: 0, text: "Help"};

//Pause Screen Buttons
var resume_button = {name: "resume_button", posx: (canvas.width/2)-160, posy: canvas.height-105, width: 150, height: 60, screen: 2, text: "Resume"};
var exit_button = {name: "exit_button", posx: (canvas.width/2)+5, posy: canvas.height-105, width: 150, height: 60, screen: 2, text: "Exit"};

//Game Over Buttons
var retry_button = {name: "retry_button", posx: (canvas.width/2)-160, posy: canvas.height-105, width: 150, height: 60, screen: 3, text: "Retry"};
var exit_button_go = {name: "exit_button_go", posx: (canvas.width/2)+5, posy: canvas.height-105, width: 150, height: 60, screen: 3, text: "Exit"};

//Help Screen Buttons
var back_button = {name: "back_button", posx: (canvas.width/2)-75, posy: canvas.height-105, width: 150, height: 60, screen: 4, text: "Back"};

//Select Ship Buttons
var select_ship_button = {name: "select_ship_button", posx: (canvas.width/2)-175, posy: canvas.height-105, width: 200, height: 60, screen: 5, text: "Start Game"};
var back_button_ss = {name: "back_button_ss", posx: (canvas.width/2)+50, posy: canvas.height-105, width: 120, height: 60, screen: 5, text: "Back"};
var next_ship_button = {name: "next_ship_button", posx: (canvas.width/2)+90, posy: 205, width: 80, height: 90, screen: 5};
var prev_ship_button = {name: "prev_ship_button", posx: (canvas.width/2)-170, posy: 205, width: 80, height: 90, screen: 5};
var next_color_button = {name: "next_color_button", posx: (canvas.width/2)+130, posy: 350, width: 80, height: 60, screen: 5};
var prev_color_button = {name: "prev_color_button", posx: (canvas.width/2)-210, posy: 350, width: 80, height: 60, screen: 5};

//Intro Screen Buttons
var continue_button = {name: "continue_button", posx: (canvas.width/2)-165, posy: canvas.height-105, width: 180, height: 60, screen: 6, text: "Continue"};
var back_button_intro = {name: "back_button_intro", posx: (canvas.width/2)+40, posy: canvas.height-105, width: 120, height: 60, screen: 6, text: "Back"};

var buttons = [music_button_on, music_button_off, sound_button_on, sound_button_off, pause_button, start_button, help_button,
resume_button, exit_button, retry_button, exit_button_go, back_button, select_ship_button, next_ship_button, prev_ship_button,
next_color_button, prev_color_button, back_button_ss, continue_button, back_button_intro];

//Function to draw the ship onto the canvas using the mouse position
function drawShip() {
	/*
	 * Rotate the ship around the planet, based on the mouse position
	 * With help from: http://stackoverflow.com/questions/5964236/how-can-i-animate-an-object-in-orbit-using-html5-canvas
	 * http://stackoverflow.com/questions/15653801/rotating-object-to-face-mouse-pointer-on-mousemove
	 * http://stackoverflow.com/questions/27766647/drag-move-an-image-inside-a-circle-circular-movement
	 */
	
	context.save();
	context.translate((canvas.width/2), (canvas.height/2));
	context.rotate(mouseangle);
	context.translate(-(canvas.width/2), -(canvas.height/2));
	drawShipImage((canvas.width/2)-32, (canvas.height/2)-(planet.height/2)-64);
	context.restore();
}

//Function to draw bullets onto the canvas
function drawBullets() {
	//Draw each bullet in the array, incrementing the position each time
	for(i = 0; i < bullets.length; i++)
	{
		drawBulletImage(bullets[i].x, bullets[i].y);
		
		//If any of the bullets collide with a carrot, delete them and increment the score
		if(carrotCollide(bullets[i])) {
			bullets.splice(i, 1);
			score += 10;
		} else {
			//Increment x and y such that it travels in the angle it was 'fired in'
			//With help from: http://stackoverflow.com/questions/11394706/inverse-of-math-atan2
			bullets[i].x += 2.5*Math.sin(bullets[i].angle);
			bullets[i].y -= 2.5*Math.cos(bullets[i].angle);
			
			//If any of the bullets go out of the canvas, delete them
			if(bullets[i].x > canvas.width || bullets[i].x < -35 || bullets[i].y > canvas.height || bullets[i].y < -35) {
				bullets.splice(i, 1);
			}
		}
	}
}

//Function to draw carrots onto the canvas
function drawCarrots() {
	//First check if we have enough carrots on the screen for the current level, if not then create some!
	if(carrots.length < levels[current_level].max_carrots) {
		for(i = 0; i < (levels[current_level].max_carrots - carrots.length); i++)
		{
			//First decide whether we're actually going to create a carrot (from a given probability)
			var create = Math.random();
			
			if(create <= levels[current_level].carrot_prob)
			{
				//First let's randomise the direction the carrot comes from
				var lr = Math.floor(Math.random() * 2); //Choose between left and right
				var y = Math.floor(Math.random() * canvas.height); //Choose a random y value
				var x = -carrot.width;
				if(lr == 1) x = canvas.width;
				
				//Then work out the angle it needs to travel in to get to the planet
				var angle = Math.atan2(x-canvas.width/2, -(y-canvas.height/2))
				
				carrots[carrots.length] = {x: x, y: y, angle: angle};
			}
		}
	}
	
	//Draw each carrot in the array, incrementing the position each time
	for(i = 0; i < carrots.length; i++)
	{
		drawCarrotImage(carrots[i].x, carrots[i].y, carrots[i].angle);
		
		//If any of the carrots collide with the planet, delete them and decrease planet health
		if(planetCollide(carrots[i])) {
			carrots.splice(i, 1);
			health -= levels[current_level].health_points;
			
			//If sound is enabled, play the explosion sound
			if(sound_enabled) {
				explosion_sound.currentTime = 0;
				explosion_sound.play();
			}
		} else {
			//If there wasn't a collision, we just need to move the carrot towards the planet
			//Firstly let's get the distance to the planet's edge using Pythagoras - http://www.purplemath.com/modules/distform.htm
			var distance = Math.sqrt(Math.pow(carrots[i].x - (canvas.width/2), 2) + Math.pow(carrots[i].y - (canvas.height/2), 2)) - planet.width/2;
			
			//Work out how much to speed up the carrot by getting a value which increases as it gets closer to the planet.
			//As we divide distance by the canvas width, the factor starts around 0.6 and gets to 1 times the defined speed.
			//This creates the effect of carrots accelerating as they get closer to the planet.
			var gravity_factor = 1 - (distance/canvas.width);
			
			//Increment x and y such that it travels in the angle it was 'fired in'
			//With help from: http://stackoverflow.com/questions/11394706/inverse-of-math-atan2
			carrots[i].x -= gravity_factor*levels[current_level].carrot_speed*Math.sin(carrots[i].angle);
			carrots[i].y += gravity_factor*levels[current_level].carrot_speed*Math.cos(carrots[i].angle);
			
			//If any of the carrots go out of the canvas, delete them
			if(carrots[i].x > (canvas.width + (carrot.width*2)) || carrots[i].x < (-(carrot.width*2)) || carrots[i].y > (canvas.height + (carrot.height*2)) || carrots[i].y < (-(carrot.width*2))) {
				carrots.splice(i, 1);
			}
		}
	}
}

//Function to draw any explosions onto the canvas
function drawExplosions() {
	//Draw each explosion in the array, decrementing it's timer each time
	for(i = 0; i < explosions.length; i++)
	{
		drawExplosionImage(explosions[i].x, explosions[i].y);
		explosions[i].timer--;
		
		//If the timer on the current explosion has reached zero, remove it from the array
		if(explosions[i].timer <= 0) explosions.splice(i, 1);
	}
}

//Helper function to draw a ship image with a particular position and size (using the global ship_type)
function drawShipImage(posx, posy, size) {
	size = size || 64;
	if(bounding_boxes) context.strokeRect(posx, posy, 64, 64);
	context.drawImage(ship_sprites[ship_color], (ship_type*64), 0, 64, 64, posx, posy, size, size);
}

//Helper function to draw a bullet image with a particular position (using the global ship_color)
function drawBulletImage(posx, posy) {
	if(bounding_boxes) context.strokeRect(posx, posy, 31, 30);
	context.drawImage(bullet_sprites, (ship_color*31), 0, 31, 30, posx, posy, 31, 30);
}

//Helper function to draw a carrot with a particular position and angle
function drawCarrotImage(posx, posy, angle) {	
	context.save();
	context.translate(posx + (carrot.width/2), posy + (carrot.height/2));
	context.rotate(angle - 90 * Math.PI/180);
	context.translate(-(posx + (carrot.width/2)), -(posy + (carrot.height/2)));
	context.drawImage(carrot, posx, posy);
	if(bounding_boxes) context.strokeRect(posx, posy, carrot.width, carrot.height);
	context.restore();
}

//Helper function to draw an explosion with a particular position
function drawExplosionImage(posx, posy) {
	context.drawImage(explosion_image, posx, posy);
}

//Function to create a bullet which will then be drawn in drawBullets()
function fireBullet() {
	//Create a bullet with the current mouse angle and a position worked out from that angle (to make it appear to fire from the ship)
	//With help from: http://stackoverflow.com/questions/11394706/inverse-of-math-atan2
	bullets[bullets.length] = {x:((Math.sin(mouseangle)*225) + canvas.width/2 - 15), y:((Math.cos(mouseangle)*-225) + canvas.height/2 - 15), angle:mouseangle};
	
	//If sound is enabled, play the shoot sound
	if(sound_enabled) {
		shoot_sound.currentTime = 0;
		shoot_sound.play();
	}
}

//Function to check whether the carrot is colliding with the planet (rect and circle collision)
function planetCollide(c) {
	//Doesn't take the carrots rotation into account, but after testing, this method seems to be accurate enough.
	//With help from: http://stackoverflow.com/questions/21089959/detecting-collision-of-rectangle-with-circle-in-html5-canvas
	 
	//First get the distances between the centres of the planet and carrot
	var distX = Math.abs((planet.posx+(planet.width/2)) - (c.x+(carrot.width/2)));
    var distY = Math.abs((planet.posy+(planet.height/2)) - (c.y+(carrot.height/2)));
	
	//Check for obvious (or not) collisions
	if (distX > (carrot.width/2 + planet.width/2)) return false;
    if (distY > (carrot.height/2 + planet.height/2)) return false;
	if (distX <= (carrot.width/2)) return true;
    if (distY <= (carrot.height/2)) return true;
	
	//Then check the carrots corners
	var dx=distX-carrot.width/2;
    var dy=distY-carrot.height/2;
    return (dx*dx+dy*dy<=((planet.height/2)*(planet.height/2)));
}

//Function to check whether the bullet is colliding with a carrot (rect/rect collision)
function carrotCollide(bullet) {	
	//Get our top/bottom/left/right values for the bullet (with a tolerance so the bullet actually 'hits' the carrot)
	var tolerance = carrot.height * 0.3;
	var top = bullet.y + tolerance;
	var bottom = bullet.y + 30 - tolerance;
	var left = bullet.x + tolerance;
	var right = bullet.x + 31 - tolerance;
	
	//Loop through each carrot and check it's position against the bullet's
	for(j = 0; j < carrots.length; j++) {
		var collision = true;
		
		if (bottom < carrots[j].y) collision = false;
		if (top > (carrots[j].y + carrot.height)) collision = false;
		if (right < carrots[j].x) collision = false;
		if (left > (carrots[j].x + carrot.width)) collision = false;
		
		//If we find a match, return true
		if(collision)
		{
			//Add an explosion to the explosions array
			explosions[explosions.length] = {x: carrots[j].x, y: carrots[j].y, timer: 30};
			
			//Remove the carrot in question
			carrots.splice(j, 1);
			
			//If sound is enabled, play the explosion sound
			if(sound_enabled) {
				explosion_sound.currentTime = 0;
				explosion_sound.play();
			}
			
			return true;
		}
	}
	
	//Otherwise return false
	return false;
}

//Helper function to draw button objects (defined above)
function drawButton(button) {
	context.strokeStyle = "#fff";
	context.fillStyle = "#fff";
	
	context.strokeRect(button.posx, button.posy, button.width, button.height);
	if(button.text != undefined) drawText(button.text, button.posx + button.width/2, (button.posy + 11 + button.height/2), 35);
}

//Helper function to draw image objects (defined above)
function drawImage(image) {
	context.drawImage(image, image.posx, image.posy);
}

//Helper function to draw text
function drawText(text, posx, posy, fontsize, align) {
	context.fillStyle = "#fff";
	context.font = (fontsize || 16) + "px arial";
	context.textAlign = align || "center";
	
	context.fillText(text, posx, posy);
}