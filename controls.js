//Carrot Invaders - controls.js - Kieran Dunbar (kid10)

var mousex = 0;
var mousey = 0;
var mouseangle = 0;
var mouseOver;
var keyMap = {27:false, 32:false, 37:false, 38:false, 39:false, 40:false, 65:false, 68:false, 83:false, 87:false};
var bulletFired = false;
var flipped = false;

//Capture the mouse move event, updating the variables as needed
function mouseMove(mouse_event) {
	mousex = (mouse_event.clientX-bounding_box.left) * (canvas.width/bounding_box.width);
	mousey = (mouse_event.clientY-bounding_box.top) * (canvas.height/bounding_box.height);
	mouseangle = Math.atan2(mouse_event.clientX-canvas.offsetLeft-canvas.width/2, -(mouse_event.clientY-canvas.offsetTop-canvas.height/2));
	
	mouseOver = mouseOverObjects(buttons);
	
	//Handle some mouse pointer styling here
	if(mouseOver != "") {
		canvas.style.cursor = "pointer";
	} else {
		canvas.style.cursor = "auto";
	}
}

//Capture the mouse click event, either processing button clicks or firing a bullet
function mouseClick(mouse_event) {
	//If we're on the main screen and we're not clicking a button, fire a bullet
	if(current_screen == 1 && mouseOver == "") {
		fireBullet();
	}
	//Otherwise find which button we're clicking and do something
	else if(mouseOver != "") {
		switch(mouseOver)
		{
			case "start_button":
				current_screen = 6;
			break;
			case "continue_button":
				current_screen = 5;
			break;
			case "help_button":
				current_screen = 4;
			break;
			case "pause_button":
				current_screen = 2;
			break;
			case "resume_button":
				current_screen = 1;
			break;
			case "exit_button":
			case "exit_button_go":
				resetGame();
				current_screen = 0;
			break;
			case "retry_button":
				resetGame();
				current_screen = 1;
			break;
			case "back_button":
			case "back_button_intro":
				current_screen = 0;
			break;
			case "back_button_ss":
				current_screen = 6;
			break;
			case "music_button_on":
			case "music_button_off":
				music_enabled = !music_enabled;
			break;
			case "sound_button_on":
			case "sound_button_off":
				sound_enabled = !sound_enabled;
			break;
			case "next_ship_button":
				if(ship_type == 9) ship_type = 0;
				else ship_type++;
			break;
			case "prev_ship_button":
				if(ship_type == 0) ship_type = 9;
				else ship_type--;
			break;
			case "next_color_button":
				if(ship_color == ship_colors.length-1) ship_color = 0;
				else ship_color++;
			break;
			case "prev_color_button":
				if(ship_color == 0) ship_color = ship_colors.length-1;
				else ship_color--;
			break;
			case "select_ship_button":
				current_screen = 1;
			break;
		}
	}
}

//Shortcut function to check whether the mouse is over any buttons defined in resources
function mouseOverObjects(objects) {
	var result = "";
	
	for(i = 0; i < objects.length; i++) {
		if(mouseOverObject(objects[i])) {
			if(objects[i].screen != undefined && objects[i].screen == current_screen) {
				result = objects[i].name;
				break;
			} else if(objects[i].screen == undefined) {
				result = objects[i].name;
				break;
			}
		}
	}
	
	return result;
}

//Helper function to check whether the mouse is over a particular object (defined in resources)
function mouseOverObject(object) {
	return mouseOverPos(object.posx, object.posy, object.width, object.height);
}

//Function to check whether the mouse is over a particular rectangle (passing position and dimensions)
function mouseOverPos(posx, posy, width, height) {
	if(mousex > posx && mousex < (posx + width) && mousey > posy && mousey < (posy + height)) {
		return true;
	} else {
		return false;
	}
}

//Capture the key down events
//Code for keeping track of multiple keypresses from: http://stackoverflow.com/questions/10655202/detect-multiple-keys-on-single-keypress-event-on-jquery
function keyDown(key_event) {
	var keyCode = (key_event == null) ? window.event.keyCode : key_event.keyCode;
    if (keyCode in keyMap) {
        keyMap[keyCode] = true;
    }
}

//Also capture the keyUp event
function keyUp(key_event) {
	var keyCode = (key_event == null) ? window.event.keyCode : key_event.keyCode;
    if(keyCode in keyMap) {
        keyMap[keyCode] = false;
		
		//If we've released the space, allow further firing of bullets
		if(keyCode == 32) bulletFired = false;
		
		//And if we've released up/down/w/s, allow further swapping
		if(keyCode == 38 || keyCode == 40 || keyCode == 83 || keyCode == 87) flipped = false;
    }
}

//Function called periodically from drawScreen() to process any key presses captured above
function checkKeys() {
	//If the left arrow key (or A) is pressed, move the ship anti-clockwise
	if(keyMap[37] || keyMap[65])
	{
		mouseangle -= 0.8 * Math.PI/180;
		
		//Correct any angles so we keep within the -Pi - Pi range
		if(mouseangle < -Math.PI) {
			mouseangle += Math.PI*2;
		} else if(mouseangle > Math.PI) {
			mouseangle -= Math.PI*2;
		}
	}
	//If the right arrow key (or D) is pressed, move the ship clockwise
	else if(keyMap[39] || keyMap[68])
	{
		mouseangle += 0.8 * Math.PI/180;
		
		//Correct any angles so we keep within the -Pi - Pi range
		if(mouseangle < -Math.PI) {
			mouseangle += Math.PI*2;
		} else if(mouseangle > Math.PI) {
			mouseangle -= Math.PI*2;
		}
	}
	//If escape is pressed, pause the game
	else if(keyMap[27])
	{
		current_screen = 2;
	}
	
	//If space has been pressed and we've not already fired a bullet
	if(keyMap[32] && !bulletFired)
	{
		fireBullet();
		bulletFired = true; //Set bulletFired to true to prevent holding down space
	}
	
	//If the up/down arrow keys (or W/S keys) are pressed, swap the ship to the other side of the planet
	if((keyMap[38] || keyMap[40] || keyMap[83] || keyMap[87]) && !flipped)
	{
		mouseangle -= Math.PI;
		
		//Correct any angles so we keep within the -Pi - Pi range
		if(mouseangle < -Math.PI) {
			mouseangle += Math.PI*2;
		} else if(mouseangle > Math.PI) {
			mouseangle -= Math.PI*2;
		}
		
		flipped = true; //Set a variable so it doesn't keep swapping
	}
}